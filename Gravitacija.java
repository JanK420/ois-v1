class Gravitacija{
    public static void main(String[] args){
        System.out.println("OIS je zakon!");
    }
    public double izracun(double visina){
        double MasaZ=5.972*Math.pow(10,24);
        double PolmerZ=6.371*Math.pow(10,6);
        double gKonst=6.674*Math.pow(0.1,11);
        double Pospesek=(gKonst*MasaZ)/((visina+PolmerZ)*(visina+PolmerZ));
        return Pospesek;
    }
}